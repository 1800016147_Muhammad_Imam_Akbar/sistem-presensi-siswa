import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public router:Router,
    public alert:AlertController
  ) { }

  ngOnInit() {
  }

  async login(){
    this.showAlert("Hallo", "Login Sukses")
    console.log("Login Sukses")
    this.router.navigate(['/tabs/tab1'])
  }

  async showAlert(header: string, message:string){
      const alert = await this.alert.create({
        header,
        message,
        buttons: ["Silahkan Lanjutkan"]
      })
      await alert.present()
  }

}
